<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style.min.css" rel="stylesheet">
</head>
<body>
<?
$userId = $_GET['user_id'];
?>
<? echo $userId; ?>
<div id="timer"></div>
<script src="js/socket.io-1.4.5.js"></script>
<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/common.js"></script>
<script src="js/main.js"></script>
<script src="js/draft.js"></script>
<?php
$SECRET_KEY = '6r16[)7z#g}i?yw{';

$authParams = array(
    'user_id'   => $userId,
    'auth_key'  => md5(uniqid('chat', true)),
    'username'  => 'Peter_'.$userId,
    'mode'      => 2,
    'ava'       => 'no_logo.png',
    'matches'   => [],
    'tm'        => time(),
);

$authParams['sig'] = generateSig($authParams, $SECRET_KEY);

function generateSig($parameters, $secretKey)
{
    ksort($parameters);
    $paramsStr = '';
    foreach ($parameters as $k => $v) {
        if ($k != 'matches')
            $paramsStr .= $k . '=' . $v;
    }

    return md5($paramsStr . $secretKey);
}
?>
<script>
    Socket.init('<?= http_build_query($authParams);?>', <?=$userId;?>);
</script>
</body>
</html>