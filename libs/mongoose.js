var mongoose    = require('mongoose');
var settings = require('../helpers/settings');
function connect() {
    mongoose.connect(settings.mongo.uri);
    var db = mongoose.connection;

    db.on('error', function (err) {
        console.log('connection error: ' + err.message);
    });
    db.once('open', function callback() {
        console.log("Connected to DB!");
    });
}
module.exports = connect;