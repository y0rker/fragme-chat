var cluster = require("cluster");
var CPUs = require("os").cpus();
var Settings = require('./helpers/settings');

if (cluster.isMaster) {
    cluster.data = {
        users: []
    };
    //Launch workers
    for (var i = 0; i < 1; i++) {
        process.env.PORT = Settings.server.port+i;
        cluster.fork();
    }
} else {
    console.log("pid " + cluster.worker.process.pid + " started");
    init(cluster.worker.process.pid);
}
function init() {
    var io = require('socket.io')(process.env.PORT);
    /* -------------MODELS-------------*/
    var chat = require('./models/chat');
    var User = require('./models/user');
    /* -------------MODULES-------------*/

    chat.init(io);
    io.on('connection', function (client) {
        var user = new User();
        user.init(client, chat);

        // Подписываемся на событие message от клиента (Приват)
        client.on('message', function (message) {
            user.message(client, message);
        });
        // Подписываемся на событие message от клиента (Матч)
        client.on('messageMatch', function (message) {
            user.messageMatch(client, message);
        });
        // Отписываемся от получание статуса онлайн/оффлайн собеседника
        client.on('unSubscribeMe', function (message) {
            user.unSubscribeMe(client, message);
        });
        // Получаем статус онлайн/оффлайн пользователя
        client.on('getStatusUser', function (message) {
            user.getStatus(client, chat, message);
        });
        // Получаем статус онлайн/оффлайн пользователей
        client.on('getStatusesUsersMatches', function (message) {
            user.getStatusesUsersMatches(client, chat, message);
        });
        // Читаем сообщения
        client.on('readMessages', function (message) {
            user.readMessages(message);
        });
        // Пользователь отключается
        client.on('disconnect', function () {
            user.disconnect(client, chat);
        });
        // Заходим в определённую комнату
        client.on('join_room', function (message) {
            user.joinRoom(client, message);
        });
        // Сообщение в комнату (Турнир)
        client.on('message_room', function (message) {
            user.messageRoom(client, message);
        });
        // Подписываемся на событие получение драфта
        client.on('draft.init', function (message) {
            var match_id = message.match_id;
            user.joinToDraft(client, match_id);
        });
    });
}