var timer = function() {
    this.delayed = {};
};
timer.prototype.set = function(user_id, callback) {
    this.delayed['timer' + user_id] = setTimeout(callback, 3000);
};
timer.prototype.delete = function(user_id) {
    clearTimeout(this.delayed['timer' + user_id]);
    delete this.delayed['timer' + user_id];
};
timer.prototype.exist = function (user_id) {
    return this.delayed['timer' + user_id] != undefined;
};
module.exports = timer;