var config = {};

config.server = {};
config.mysql = {};
config.mongo = {};
config.redis = {};

// Порт
config.server.port = 5000;
config.server.host = 'http://fragme.org/';

// Redis
config.redis.prefix = "chat";
config.redis.host = "localhost";
config.redis.port = 6379;

// Mysql
config.mysql.connectionLimit = 500;
config.mysql.host = '127.0.0.1';
config.mysql.user = 'root';
config.mysql.password = '';
config.mysql.db = 'fragme_production1';

// Mongodb
config.mongo.uri = 'mongodb://193.124.176.249:27017/chat';

module.exports = config;
