function replaceUrls(text) {
    var regex_url = /([--:\w?@%&+~#=]*\.[a-z]{2,4}\/{0,2})((?:[?&](?:\w+)=(?:\w+))+|[--:\w?@%&+~#=]+)?/ig;
    return text.replace(regex_url, function (str, url) {
        if (url.startsWith('http://fragme.org') || url.startsWith('https://fragme.org') || url.startsWith('https://new.vk.com') || url.startsWith('https://vk.com')) {
            return '<a target="_blank" href="' + str + '">' + str + '</a>';
        } else {
            return str;
        }
    });
}
module.exports = replaceUrls;