var Draft = {
    tryConnect: function (match_id) {
        if (Socket.socket != null) {
            this.init();
        } else {
            var try_connect = setTimeout(function () {
                if (Socket.socket != null) {
                    clearTimeout(try_connect);
                    Draft.init(match_id);
                }
            }, 500);
        }
    },
    init: function(match_id) {
        // Пришел статус онлайн/оффлайн пользователя
        Socket.socket.on('draft.fetch', function (data) {
            console.log(data);
            $("#draft").html(data.html);
        });
        Socket.socket.on('draft.send', function (data) {
            console.log(data);
        });
        Socket.socket.emit("draft.fetch", {match_id: parseInt(match_id)});
    },
    setReady: function() {
        Socket.socket.emit("draft.ready");
        return false;
    },
    choose: function(choose_id) {
        Socket.socket.emit("draft.choose", {
            choose_id: choose_id
        });
        return false;
    }
};