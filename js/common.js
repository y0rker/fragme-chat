if (!window._ua) {
    var _ua = navigator.userAgent.toLowerCase();
}

var browser = {
    version: (_ua.match( /.+(?:me|ox|on|rv|it|era|opr|ie)[\/: ]([\d.]+)/ ) || [0,'0'])[1],
    opera: (/opera/i.test(_ua) || /opr/i.test(_ua)),
    msie: (/msie/i.test(_ua) && !/opera/i.test(_ua) || /trident\//i.test(_ua)),
    msie6: (/msie 6/i.test(_ua) && !/opera/i.test(_ua)),
    msie7: (/msie 7/i.test(_ua) && !/opera/i.test(_ua)),
    msie8: (/msie 8/i.test(_ua) && !/opera/i.test(_ua)),
    msie9: (/msie 9/i.test(_ua) && !/opera/i.test(_ua)),
    mozilla: /firefox/i.test(_ua),
    chrome: /chrome/i.test(_ua),
    safari: (!(/chrome/i.test(_ua)) && /webkit|safari|khtml/i.test(_ua)),
    iphone: /iphone/i.test(_ua),
    ipod: /ipod/i.test(_ua),
    iphone4: /iphone.*OS 4/i.test(_ua),
    ipod4: /ipod.*OS 4/i.test(_ua),
    ipad: /ipad/i.test(_ua),
    android: /android/i.test(_ua),
    bada: /bada/i.test(_ua),
    mobile: /iphone|ipod|ipad|opera mini|opera mobi|iemobile|android/i.test(_ua),
    msie_mobile: /iemobile/i.test(_ua),
    safari_mobile: /iphone|ipod|ipad/i.test(_ua),
    opera_mobile: /opera mini|opera mobi/i.test(_ua),
    opera_mini: /opera mini/i.test(_ua),
    mac: /mac/i.test(_ua),
    search_bot: /(yandex|google|stackrambler|aport|slurp|msnbot|bingbot|twitterbot|ia_archiver|facebookexternalhit)/i.test(_ua),
    width: 0
};

function rs(html, repl) {
    each (repl, function(k, v) {
        html = html.replace(new RegExp('%' + k + '%', 'g'), v);
    });
    return html;
}
//
//  Arrays, objects
//
function each(object, callback) {
    if (!isObject(object) && typeof object.length !== 'undefined') {
        for (var i = 0, length = object.length; i < length; i++) {
            var value = object[i];
            if (callback.call(value, i, value) === false) break;
        }
    } else {
        for (var name in object) {
            if (!Object.prototype.hasOwnProperty.call(object, name)) continue;
            if (callback.call(object[name], name, object[name]) === false)
                break;
        }
    }
    return object;
}

function isObject(obj) { return Object.prototype.toString.call(obj) === '[object Object]' && !(browser.msie8 && obj && obj.item !== 'undefined' && obj.namedItem !== 'undefined'); }

//
// Cookies
//

var _cookies;
function _initCookies() {
    _cookies = {};
    var ca = document.cookie.split(';');
    var re = /^[\s]*([^\s]+?)$/i;
    for (var i = 0, l = ca.length; i < l; i++) {
        var c = ca[i].split('=');
        if (c.length == 2) {
            _cookies[c[0].match(re)[1]] = unescape(c[1].match(re) ? c[1].match(re)[1] : '');
        }
    }
}
function getCookie(name) {
    _initCookies();
    return _cookies[name];
}
function setCookie(name, value, days) {
    var expires = '';
    var path = '; path=/';
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        expires = '; expires='+date.toGMTString();
    }
    document.cookie = name + '='+encodeURI(value) + expires + path;
}

//
// Arrays
//
function searchElem(array, id) {
    for (var key in array) {
        if (array.hasOwnProperty(key)) {
            if (array[key].id == id)
                return key;
        }
    }
    return null;
}

//
// Date
//
function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}