$(document).ready(function() {
    browser.width = $(window).width();
    //var n_message = $(".user-status-bar-dialog-new-message a.user-status-bar-dialog-link, .user-status-bar-more a.user-status-bar-bars");
    //blink(n_message);
    $("#chat_room input.chat-input").keyup(function(e) {
        if (e.keyCode == '13') {
            // Отправляем содержимое input'а, закодированное в escape-последовательность
            //socket.send(escape(document.querySelector('#input').value));
            // Очищаем input
            //document.querySelector('#input').value = '';
            chat.send_msg(this.value);
            this.value = '';
        }
    });
});
var Socket = {
    socket: null,
    connected: false,
    user_id: 0,
    info: {
        user_id: 0,
        username: '',
        avatar: '',
        mode: 0
    },
    init: function(query, user_id) {

        ////
        $("#status").click(function() {
            var us_id = $("#user_id").val();
            FastChat.newMessageBox(parseInt(us_id),'User ' + us_id, {opened: true, divReturn: false});
        });
        ////

        this.user_id = user_id;
        var randomNumber = Math.random() >= 0.5;
        var port = 4000;
        if (randomNumber)
            port = 4001;
        this.socket = io.connect('ws://localhost:5000/', { query: query });
        this.socket.on('connect', function (e) {
            Socket.connected = true;
            console.log("Connected to chat! Port " +port);
            if (query != null)
                FastChat.init();
        });
        this.socket.on('disconnect', function (e) {
            Socket.connected = false;
            console.log("Disconnected to chat!");
            console.log(e);
        });
        // Пришла информация о пользователе
        this.socket.on('info', function (data) {
            Socket.info.user_id = data.user_id;
            Socket.info.username = data.username;
            Socket.info.avatar = data.avatar;
            Socket.info.mode = data.mode;
        });
    }
};

var FastChat = {
    options: {
        width: 265
    },
    inited: false,
    openedChats: [],
    init: function() {
        if (this.inited)
            return;
        // Главная банка
        this.fastChatBox = $("#user-status-bar");

        // Посылаем что пользователь онлайн (Собеседник запросил статус)
        Socket.socket.on('getStatusUser', function (data) {
            console.log('getStatusUser');
            console.log(data);
            var key = FastChat.findKey(data.user_id);
            if (FastChat.openedChats[key] != undefined) {
                var class_online = 'user-status-bar_dialog-status--off';
                if (data.online == true)
                    class_online = 'user-status-bar_dialog-status--on';
                FastChat.openedChats[key].dom.find('.user-status-bar_dialog-status').removeClass('user-status-bar_dialog-status--off').removeClass('user-status-bar_dialog-status--on').addClass(class_online);
                FastChat.openedChats[key].dom.find('.user-status-bar_dialog-status').removeClass('user-status-bar_dialog-status--off').removeClass('user-status-bar_dialog-status--on').addClass(class_online);
            }
            if (data.type != 'complete')
                Socket.socket.emit("getStatusUser", {user_id: data.user_id, type: data.type});
        });

        // Слушаем сокеты
        // Пришло сообщение
        Socket.socket.on('message', function (data) {
            if (data.from_id != Socket.info.user_id) {
                var div = FastChat.newMessageBox(data.user_id, data.nick, {
                    opened: false,
                    divReturn: true
                });
                FastChat.getStatusUser(data.user_id);
                // Добавляем в чатик
                if (Socket.user_id != data.from_id)
                    div.find('.tse-content').append(data.html);
                // Если чат закрыт, то показываем моргание
                for (var key_in in FastChat.openedChats) {
                    if (FastChat.openedChats.hasOwnProperty(key_in)) {
                        if (FastChat.openedChats[key_in].id == data.user_id) {
                            if (!FastChat.openedChats[key_in].opened) {
                                div.addClass("user-status-bar-dialog-new-message");
                                //blink(div.find('.user-status-bar-dialog-link'), key_in, true);
                                // Так же прибавляем счетчик непрочитанных
                                FastChat.openedChats[key_in].unread++;
                                FastChat.openedChats[key_in].offset++;
                                div.find('.user-status-bar-dialog-count').removeClass('user-status-bar-dialog-count--hidden').text(FastChat.openedChats[key_in].unread);
                                if (Socket.user_id != data.from_id) {
                                    FastChat.playSound();
                                    // Будущий звук
                                    /*var state_mes = getCookie('user_' + data.id);
                                     if (state_mes != undefined) {
                                     if (data.room_size >= parseInt(state_mes))
                                     setCookie('user_' + data.id, 1, -1);
                                     } else {
                                     FastChat.playSound();
                                     setCookie('user_' + data.id, data.room_size);
                                     }*/
                                }
                            } else if (Socket.user_id != data.from_id) {
                                div.find(".tse-scroll-content").scrollTop(div.find('.tse-content').height());
                                FastChat.readMessages(data.from_id);
                            }
                            break;
                        }
                    }
                }
            }
        });

        // Пришло сообщение в матч
        Socket.socket.on('message_match', function (data) {
            var match_id = "match_"+data.match_id;
            // Если чат закрыт, то показываем моргание
            for (var key_in in FastChat.openedChats) {
                if (FastChat.openedChats.hasOwnProperty(key_in)) {
                    if (FastChat.openedChats[key_in].id == match_id) {
                        // Добавляем в чатик
                        if (Socket.user_id != data.from_id) {
                            FastChat.openedChats[key_in].dom.find('.tse-content').append(data.html);
                            if (!FastChat.openedChats[key_in].opened) {
                                // Будущий звук
                            } else {
                                FastChat.openedChats[key_in].dom.find(".tse-scroll-content").scrollTop(FastChat.openedChats[key_in].dom.find('.tse-content').height());
                            }
                        }
                        break;
                    }
                }
            }
        });

        // Пришел статус онлайн/оффлайн пользователя
        Socket.socket.on('statusUser', function (data) {
            console.log(data);
            var key = FastChat.findKey(data.user_id);
            if (FastChat.openedChats[key] != undefined) {
                var class_online = 'user-status-bar_dialog-status--off';
                if (data.online == true)
                    class_online = 'user-status-bar_dialog-status--on';
                FastChat.openedChats[key].dom.find('.user-status-bar_dialog-status').removeClass('user-status-bar_dialog-status--off').removeClass('user-status-bar_dialog-status--on').addClass(class_online);
                FastChat.openedChats[key].dom.find('.user-status-bar_dialog-status').removeClass('user-status-bar_dialog-status--off').removeClass('user-status-bar_dialog-status--on').addClass(class_online);
            }
        });

        // Пришли онлайн игроков в матче
        Socket.socket.on('match_online_member', function (data) {
            for (var key_d in data) {
                if (data.hasOwnProperty(key_d)) {
                    if (data[key_d].online)
                        $("#match_"+data[key_d].match_id+"_member_" + data[key_d].member_id).find('.user-status-bar_dialog-status').removeClass('user-status-bar_dialog-status--off').addClass('user-status-bar_dialog-status--on');
                    else
                        $("#match_"+data[key_d].match_id+"_member_" + data[key_d].member_id).find('.user-status-bar_dialog-status').removeClass('user-status-bar_dialog-status--on').addClass('user-status-bar_dialog-status--off');
                    if (data[key_d].type != 'complete')
                        Socket.socket.emit("match_im_online", {match_id: data[key_d].match_id, type: data[key_d].type});
                }
            }
        });

        // Пришли онлайн игроков в матче
        Socket.socket.on('matchStatusUsers', function (data) {
            var match_id = data.match_id;
            for (var key_d in data.users) {
                if (data.users.hasOwnProperty(key_d)) {
                    if (data.users[key_d].status)
                        $("#match_"+match_id+"_member_" + data.users[key_d].user_id).find('.user-status-bar_dialog-status').removeClass('user-status-bar_dialog-status--off').addClass('user-status-bar_dialog-status--on');
                    else
                        $("#match_"+match_id+"_member_" + data.users[key_d].user_id).find('.user-status-bar_dialog-status').removeClass('user-status-bar_dialog-status--on').addClass('user-status-bar_dialog-status--off');
                }
            }
        });

        // Если не прочитанны сообщения
        var users = $(".user-status-bar_dialog--user");
        var finder = [];
        users.each(function(i,elem) {
            var unread = $(elem).find('.user-status-bar-dialog-count').text();
            var chat = {
                dom: $(elem),
                id: elem.id.substr(15),
                opened: false,
                unread: parseInt(unread),
                offset: 0,
                match: false,
                load: true
            };
            finder.push(chat.id);
            var wrap_jq = $("#fast_chat_peer_" + chat.id);
            wrap_jq.find('.chat-box-wrapper-inner').TrackpadScrollEmulator();
            FastChat.openedChats.push(chat);
            FastChat.countChats++;
            if ((FastChat.countChats + 1) * FastChat.options.width < browser.width) {
                wrap_jq.show();
            }
            FastChat.getStatusUser(chat.id);
        });

        // Если открыты матчи
        var matches = $(".user-status-bar_dialog--match");
        matches.each(function(i,elem) {
            var chat = {
                dom: $(elem),
                id: elem.id.substr(15),
                opened: false,
                unread: 0,
                offset: 0,
                match: true,
                load: true
            };

            var wrap_jq = $("#fast_chat_peer_" + chat.id);
            wrap_jq.find('.chat-box-wrapper-inner').TrackpadScrollEmulator();
            FastChat.openedChats.push(chat);
            FastChat.countChats++;
            if ((FastChat.countChats + 1) * FastChat.options.width < browser.width) {
                wrap_jq.show();
            }
            FastChat.getStatusesMatch(parseInt(elem.id.substr(21)), [2,3]);
        });

        this.inited = true;

        // Вытаскиваем из кук открытые чатбоксы
        var opened_chats_str = getCookie('fastChatOpened');
        if (opened_chats_str == undefined || opened_chats_str == "")
            return;
        var opened_chats_arr = JSON.parse(opened_chats_str);
        for (var key in opened_chats_arr) {
            if (opened_chats_arr.hasOwnProperty(key)) {
                if ($.inArray(parseInt(opened_chats_arr[key].id), finder) == -1) {
                    opened_chats_arr[key].opened = "";
                    FastChat.openMessageBox(opened_chats_arr[key], false);
                }
            }
        }
    },
    // Отрабатываем нажатие на Enter у input
    watchEnter: function(id, e, th) {
        if (/([^\s])/.test(th.value)) {
            if (e.keyCode == '13') {
                if (th.value.length > 255) {
                    return;
                }
                FastChat.sendMessageToUser(th.value, id);
                th.value = '';
            }
        }
    },
    // Отрабатываем нажатие на Enter у input
    watchEnterMatch: function(id, e, th) {
        if (/([^\s])/.test(th.value)) {
            if (e.keyCode == '13') {
                if (th.value.length > 255) {
                    return;
                }
                FastChat.sendMessageToMatch(th.value, id);
                th.value = '';
            }
        }
    },
    fastChatBox: null,
    countChats: 0,
    // Новый чатбокс
    newMessageBox: function(id, name, boxOptions) {
        // Если помещается на экран
        if ((this.countChats + 1) * this.options.width < browser.width) {
            var dom_message_box = $("#fast_chat_peer_"+id);
            if (dom_message_box.length == 0) {
                var options = {
                    id: id,
                    name: name
                };
                if (boxOptions.opened) {
                    dom_message_box = this.openMessageBox(options, true);
                }
                else {
                    dom_message_box = this.openMessageBox(options, false);
                }
                if (dom_message_box !== undefined && dom_message_box.length == 1) {
                    dom_message_box.find('.chat-box-wrapper-inner').TrackpadScrollEmulator();
                    this.countChats++;

                    // Сохраняем в куки
                    this.saveChatCookie(options);

                    if (boxOptions.divReturn == true)
                        return dom_message_box;
                    else
                        return false;
                }
                return false;
            } else {
                dom_message_box.find('.chat-box-wrapper-inner').TrackpadScrollEmulator();
                if (boxOptions.opened) {
                    FastChat.openUpMessageBox(id);
                }
                if (boxOptions.divReturn == true)
                    return dom_message_box;
                else
                    return false;
            }
            // Если нет
        } else {
            // Создаем доп. меню
        }
        return false;
    },
    saveChatCookie: function(options) {
        // Запоминаем в кукисы
        var opened_chats_str = getCookie('fastChatOpened');
        var opened_chats_arr = [];
        if (opened_chats_str != undefined && opened_chats_str != "")
            opened_chats_arr = JSON.parse(opened_chats_str);

        var key = searchElem(opened_chats_arr, options.id);

        if (key == null) {
            var option_for_cookie = {
                id: options.id,
                name: options.name
            };
            opened_chats_arr.push(option_for_cookie);
            opened_chats_str = JSON.stringify(opened_chats_arr);
            setCookie('fastChatOpened', opened_chats_str, 365);
        }
    },
    // Открываем чатбокс
    openMessageBox: function(options, opened) {
        var key = this.findKey(options.id);
        if (key == null) {
            if (options.unread == undefined) {
                options.unread = "";
            }
            var wrap = rs(FastChat.tplBox, options);
            this.fastChatBox.append(wrap);
            var wrap_jq = $("#fast_chat_peer_" + options.id);
            var chat = {
                dom: wrap_jq,
                id: options.id,
                opened: false,
                unread: 0,
                offset: 0,
                match: false,
                load: true,
                blink: null
            };
            if (options.unread != undefined) {
                chat.unread = options.unread;
            }
            if (options.offset != undefined) {
                chat.offset = options.offset;
            }
            wrap_jq.find('.chat-box-wrapper-inner').TrackpadScrollEmulator();
            this.openedChats.push(chat);
            var key_in = this.findKey(options.id);
            if (options.unread > 0) {
                blink(wrap_jq.find('.user-status-bar-dialog-link'), key_in, true);
            }
            if (opened) {
                this.openUpMessageBox(options.id);
            }
            this.getStatusUser(options.id);
            return wrap_jq;
        }
        return null;
    },
    // Закрываем чатбокс
    closeMessageBox: function(id) {
        var curr_box = $("#fast_chat_peer_"+id);
        if (curr_box == undefined || (curr_box !== undefined && curr_box.length == 0))
            return false;
        // Удаляем из кук
        var opened_chats_str = getCookie('fastChatOpened');
        var opened_chats_arr = [];
        if (opened_chats_str != undefined && opened_chats_str != "")
            opened_chats_arr = JSON.parse(opened_chats_str);
        var key = searchElem(opened_chats_arr, id);

        opened_chats_arr.splice(key, 1);
        if (opened_chats_arr.length != 0)
            opened_chats_str = JSON.stringify(opened_chats_arr);
        else
            opened_chats_str = "";
        setCookie('fastChatOpened', opened_chats_str, 365);
        this.countChats--;
        curr_box.remove();

        // Удаляем из массива
        for (var key_in in this.openedChats) {
            if (this.openedChats.hasOwnProperty(key_in)) {
                if (this.openedChats[key_in].id == id) {
                    delete this.openedChats[key_in];
                    break;
                }
            }
        }

        // Отписываемся от получание статуса онлайн/оффлайн собеседника
        this.unSubscribeMe(id);

        return false;
    },
    toggleMessageBoxWrapper: function(id) {
        for (var key in this.openedChats) {
            if (this.openedChats.hasOwnProperty(key)) {
                if (this.openedChats[key].id == id) {
                    // Если открыт - закрываем
                    if (this.openedChats[key].opened) {
                        this.closeDownMessageBox(id, key);
                        // Если закрыт - открываем
                    } else {
                        this.openUpMessageBox(id, key);
                    }
                }
            }
        }
        return false;
    },
    closeDownMessageBox: function(id, key) {
        // Если пришли без ключа к массиву - ищем
        if (key == undefined) {
            key = this.findKey(id);
        }
        if (this.openedChats[key].opened) {
            this.openedChats[key].opened = false;
            this.openedChats[key].dom.find('.user-chat-box-wrapper').removeClass("user-chat-box-wrapper--opened");
        }
        return false;
    },
    openUpMessageBox: function(id, key) {
        // Если пришли без ключа к массиву - ищем
        for (var key_in in this.openedChats) {
            if (this.openedChats.hasOwnProperty(key_in)) {
                if (this.openedChats[key_in].id == id) {
                    key = key_in;
                } else {
                    this.closeDownMessageBox(this.openedChats[key_in].id, key_in);
                }
            }
        }

        if (!this.openedChats[key].opened) {
            if (this.openedChats[key].load) {
                // Грузим сообщения
                if (this.openedChats[key].match)
                    FastChat.loadMoreMatch(id, key);
                else
                    FastChat.loadMore(id, key);
            }

            // Если есть не прочитанные сообщения
            if (this.openedChats[key].unread > 0) {
                this.readMessages(id);
                if (!this.openedChats[key].load) {
                    this.openedChats[key].unread = 0;
                }
            }

            this.openedChats[key].dom.find('.user-status-bar-dialog-count').addClass('user-status-bar-dialog-count--hidden').text('');

            this.openedChats[key].opened = true;
            this.openedChats[key].dom.find('.user-chat-box-wrapper').addClass("user-chat-box-wrapper--opened").removeClass('user-status-bar-dialog-new-message');
            this.openedChats[key].dom.find('.user-status-bar-dialog-link').css('background-color', '');
            this.openedChats[key].dom.find(".tse-scroll-content").scrollTop(this.openedChats[key].dom.find('.tse-content').height());
            clearTimeout(this.openedChats[key].blink);
            this.openedChats[key].blink = null;
        }
        this.openedChats[key].dom.find('.chat-box-input-inner').focus();
        return false;
    },
    // Посылаем сообщение пользователю
    sendMessageToUser: function(message, id) {
        if (Socket.info.id != 0 && Socket.info.username != '') {
            var options = {
                user_id: Socket.info.user_id,
                username: Socket.info.username,
                message: message,
                rand: Math.random() + id
            };
            var wrap = rs(FastChat.tplMessage, options);
            var key = FastChat.findKey(id);
            FastChat.openedChats[key].dom.find('.tse-content').append(wrap);
            FastChat.openedChats[key].dom.find(".tse-scroll-content").scrollTop(FastChat.openedChats[key].dom.find('.tse-content').height());
            Socket.socket.emit("message", {message: message, user_id: id, rand: options.rand});
        }
    },
    // Посылаем сообщение в чат матча
    sendMessageToMatch: function(message, id) {
        if (Socket.info.id != 0 && Socket.info.username != '') {
            var options = {
                user_id: Socket.info.user_id,
                username: Socket.info.username,
                message: message,
                rand: Math.random() + id
            };
            var wrap = rs(FastChat.tplMessage, options);
            var key = FastChat.findKey("match_" + id);
            FastChat.openedChats[key].dom.find('.tse-content').append(wrap);
            FastChat.openedChats[key].dom.find(".tse-scroll-content").scrollTop(FastChat.openedChats[key].dom.find('.tse-content').height());
            Socket.socket.emit("message_match", {message: message, match_id: id, rand: options.rand});
        }
    },
    loadMore: function(user_id, key) {
        if (key == undefined) {
            key = this.findKey(user_id);
        }

        var offset = FastChat.openedChats[key].offset;
        $.ajax({
            type: 'GET',
            url: '/chat/getchatmessages/',
            data: 'user_id=' + user_id + '&offset=' + offset,
            success: function(data){
                if (data != "") {
                    var messages = jQuery.parseJSON(data);
                    var dom_content = FastChat.openedChats[key].dom.find('a.chat-box-more');
                    messages['messages'].forEach(function (item, i, arr) {
                        dom_content.after(item);
                    });
                    if (messages['loadMore']) {
                        dom_content.css('display', 'block');
                    } else {
                        dom_content.hide();
                    }

                    var scroll_top = 0;
                    if (FastChat.openedChats[key].load == true) {
                        scroll_top = FastChat.openedChats[key].dom.find('.tse-content').height();
                    }
                    FastChat.openedChats[key].dom.find(".tse-scroll-content").scrollTop(scroll_top);

                    // Скидываем счетчик прочтений
                    FastChat.openedChats[key].unread = 0;
                    FastChat.openedChats[key].offset += 10;
                    FastChat.openedChats[key].load = false;
                }
            }
        });
    },
    loadMoreMatch: function(match_id, key) {
        if (key == undefined) {
            key = this.findKey(match_id);
        }

        var offset = FastChat.openedChats[key].offset;
        $.ajax({
            type: 'GET',
            url: '/chat/getmatchmessages/',
            data: 'match_id=' + parseInt(match_id.substr(6)) + '&offset=' + offset,
            success: function(data){
                if (data != "") {
                    var messages = jQuery.parseJSON(data);
                    var dom_content = FastChat.openedChats[key].dom.find('a.chat-box-more');
                    messages['messages'].forEach(function (item, i, arr) {
                        dom_content.after(item);
                    });
                    if (messages['loadMore']) {
                        dom_content.css('display', 'block');
                    } else {
                        dom_content.hide();
                    }

                    var scroll_top = 0;
                    if (FastChat.openedChats[key].load == true) {
                        scroll_top = FastChat.openedChats[key].dom.find('.tse-content').height();
                    }

                    FastChat.openedChats[key].dom.find(".tse-scroll-content").scrollTop(scroll_top);
                    FastChat.openedChats[key].offset += 10;
                    FastChat.openedChats[key].unread = 0;
                    FastChat.openedChats[key].load = false;
                }
            }
        });
    },
    getStatusUser: function(user_id) {
        Socket.socket.emit("getStatusUser", {user_id: parseInt(user_id), type: 'ping'});
    },
    getStatusesMatch: function(match_id, users_id) {
        Socket.socket.emit("getStatusesMatch", {users_id: users_id, match_id: match_id});
    },
    // Отписываемся от получание статуса онлайн/оффлайн собеседника
    unSubscribeMe: function(user_id) {
        Socket.socket.emit("unSubscribeMe", {user_id: user_id});
    },
    readMessages: function(user_id) {
        Socket.socket.emit("readMessages", {user_id: user_id});
    },
    findKey: function(id) {
        for (var key in this.openedChats) {
            if (this.openedChats.hasOwnProperty(key)) {
                if (this.openedChats[key].id == id) {
                    return key;
                }
            }
        }
        return null;
    },
    playSound: function() {
        //alert('SOUND');
        //console.log('sound');
        //document.getElementById("soundMessage").innerHTML=
        //    "<embed src='/sound/message.mp3' hidden=true autostart=true loop=false>";
    },
    // Шаблон чатбокса
    //tplBox: '<div class="chat-box" id="fast_chat_peer%id%"><a href="#" class="chat-box-title">%name%</a><div class="chat-box-wrapper"><a class="chat-box-close" style="position: absolute; top: 0; right: 0;" onclick="FastChat.closeMessageBox(%id%);">Закрыть</a><div class="chat-box-wrapper-inner"></div><div class="chat-box-input"><input type="text" onkeydown="FastChat.watchEnter(%id%,event, this);"></div></div></div></div>',
    tplBox: '<div class="user-status-bar_dialog user-status-bar_dialog--user" id="fast_chat_peer_%id%">' +
    '<div class="user-chat-box-wrapper">' +
    '<div class="chat-box-wrapper_header">' +
    '<div class="user-status-bar_dialog-status"></div>' +
    '<span class="user-status-bar_dialog-name">%name%</span>' +
    '<a href="#" class="user-status-bar_dialog-close user-status-bar_dialog-close--upper" onclick="return FastChat.closeDownMessageBox(%id%);"><i class="fa fa-arrow-down"></i></a>' +
    '</div>' +
    '<div class="chat-box-wrapper-inner tse-scrollable">' +
    '<div class="tse-content">' +
    '<a class="chat-box-more" onclick="FastChat.loadMore(%id%);">Показать ещё</a>' +
    '</div>' +
    '</div>' +
    '<div class="chat-box-input"><input type="text" onkeyup="FastChat.watchEnter(%id%, event, this);" class="chat-box-input-inner"></div>' +
    '</div>' +
    '<a class="user-status-bar_dialog-link" onclick="return FastChat.toggleMessageBoxWrapper(%id%);">' +
    '<div class="user-status-bar_dialog-status"></div>' +
    '<div class="user-status-bar_dialog-name">%name%<span class="user-status-bar-dialog-count user-status-bar-dialog-count--hidden">%unread%</span></div>' +
    '</a>' +
    '<a class="user-status-bar_dialog-close" onclick="return FastChat.closeMessageBox(%id%);"><i class="fa fa-times"></i></a>' +
    '</div>',
    tplMessage: '<div class="chat-box-item chat-box-mini-item" id="unique_%rand%">' +
    '<p class="message">' +
    '<a href="/users/%user_id%/" class="name">%username%</a>' +
    '<span class="message-clock"><i class="fa fa-clock-o"> Только что</i></span>' +
    '<span class="message-text">%message%</span>' +
    '</p>' +
    '</div>'
};