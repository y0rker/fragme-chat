var TourneyChat = {
    dom_chat: null,
    roomId: null,
    inited: false,
    offset: 0,
    tryConnect: function (roomId, offset) {
        this.roomId = roomId;
        this.dom_chat = $("#chat-big-box-" + this.roomId);
        this.dom_chat.TrackpadScrollEmulator();
        this.dom_chat.find(".tse-scroll-content").scrollTop(this.dom_chat.find('.tse-content').height());
        if (Socket.socket != null) {
            this.init();
        } else {
            var try_connect = setTimeout(function () {
                if (Socket.socket != null) {
                    clearTimeout(try_connect);
                    TourneyChat.init(offset);
                }
            }, 500);
        }
    },
    init: function(offset) {
        this.offset = offset;
        //TourneyChat.dom_chat.find('.tse-content').empty();
        Socket.socket.emit("join_room", {roomId: this.roomId});

        // Пришло сообщение в чат
        if (!this.inited) {
            this.inited = true;
            Socket.socket.on('message_room', function (data) {
                if (data.roomId != undefined && data.guest == undefined) {
                    $('#chat-input-' + TourneyChat.roomId).removeAttr('disabled');
                }
                var options = {};
                if (Socket.user_id != data.id) {
                    options.can_private = data.can_private;
                    if (data.can_private != "") {
                        options.class_private = "chat-box-mini-item-can-private";
                        if (Socket.info.mode == 2)
                            options.can_delete = data.can_delete;
                        else
                            options.can_delete = '';
                        data.html = rs(data.html, options);
                        TourneyChat.dom_chat.find('.tse-content').show().append(data.html);
                        TourneyChat.dom_chat.find('.loader').hide();
                        TourneyChat.dom_chat.find(".tse-scroll-content").scrollTop(TourneyChat.dom_chat.find('.tse-content').height());
                        //$(document).foundation();
                        //resizeTourneyChat();
                    }
                } else {
                    options.can_private = "";
                    options.class_private = "";
                }
            });
        }
    },
    loadMore: function () {
        $.ajax({
            type: 'GET',
            url: '/chat/gettourneymessages/',
            data: 'room_id=' + TourneyChat.roomId + '&offset=' + TourneyChat.offset,
            success: function (data) {
                if (data != "") {
                    var messages = jQuery.parseJSON(data);
                    var dom_content = TourneyChat.dom_chat.find('a.chat-box-more');
                    messages['messages'] = messages['messages'].reverse();
                    messages['messages'].forEach(function (item, i, arr) {
                        dom_content.after(item);
                    });
                    if (messages['loadMore']) {
                        dom_content.css('display', 'block');
                    } else {
                        dom_content.hide();
                    }
                    var scroll_top = 0;
                    if (TourneyChat.offset == 0) {
                        scroll_top = TourneyChat.dom_chat.find('.tse-content').height();
                    }
                    TourneyChat.dom_chat.find(".tse-scroll-content").scrollTop(scroll_top);
                    TourneyChat.offset += 20;
                }
            }
        });
    },
    // Отрабатываем нажатие на Enter у input
    watchEnter: function (e, th) {
        if (e.keyCode == '13') {
            if (/([^\s])/.test(th.value)) {
                TourneyChat.sendMessageToRoom(th.value);
            }
            th.value = '';
        }

    },
    // Посылаем сообщение пользователю
    sendMessageToRoom: function (message) {
        if (Socket.info.id != 0 && Socket.info.username != '') {
            var options = {
                user_id: Socket.info.user_id,
                username: Socket.info.username,
                message: message,
                rand: Math.random() + this.roomId,
                avatar: Socket.info.avatar
            };
            var wrap = rs(TourneyChat.tplMessage, options);
            TourneyChat.dom_chat.find('.tse-content').append(wrap);
            TourneyChat.dom_chat.find(".tse-scroll-content").scrollTop(TourneyChat.dom_chat.find('.tse-content').height());
            Socket.socket.emit("message_room", {message: message, roomId: this.roomId});
        }
    },
    tplMessage: '<div class="chat-box-item" id="unique_%rand%">' +
    '<img src="%avatar%">' +
    '<p class="message">' +
    '<a href="/users/%user_id%/" class="name">%username%</a>' +
    '<span class="message-clock"><i class="fa fa-clock-o"> Только что</i></span>' +
    '<span class="message-text">%message%</span>' +
    '</p>' +
    '</div>'
};