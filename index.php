<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style.min.css" rel="stylesheet">
</head>
<body>
<?
$userId = $_GET['user_id'];
$matchId = 1;
$match_id = "'match_".$matchId."'";
?>
<? echo $userId; ?>
<div id="timer"></div>
<div id="user-status-bar" class="user-status-bar">
    <div class="user-status-bar_dialog user-status-bar_dialog--match" id="fast_chat_peer_match_1">
        <div class="user-chat-box-wrapper user-chat-box-wrapper--match">
            <div class="chat-box-wrapper-inner-chat">
                <div class="chat-box-wrapper_header chat-box-wrapper_header--match">
                    <i class="fa fa-chat-box fa-bolt"></i>
                    <span class="user-status-bar_dialog-name">Match</span>
                </div>
                <div class="chat-box-wrapper-inner tse-scrollable">
                    <div class="tse-content">
                        <a class="chat-box-more">Больше</a>
                    </div>
                </div>
                <div class="chat-box-input chat-box-input--match">
                    <input type="text" onkeyup="FastChat.watchEnterMatch(<?=$matchId;?>, event, this);" class="chat-box-input-inner">
                </div>
            </div>
            <div class="chat-box-online">
                <div class="chat-box-online_header">
                    <a href="" class="btn-m btn-m--blue">Результаты</a>
                    <a href="#" class="user-status-bar_online" onclick="return FastChat.closeDownMessageBox(<?=$match_id;?>);">
                        <i class="fa fa-arrow-down"></i>
                    </a>
                    <a href="" class="user-status-bar_online">
                        <i class="fa fa-sitemap"></i>
                    </a>
                </div>
                <div id="match_<?=$matchId;?>_member_2" class="chat-box-online_item">
                    <div class="user-status-bar_dialog-status"></div>
                    <a href="#" class="name chat-box-online-line score-loose-border">User 2</a>
                </div>
                <div id="match_<?=$matchId;?>_member_3" class="chat-box-online_item">
                    <div class="user-status-bar_dialog-status"></div>
                    <a href="#" class="name chat-box-online-line score-loose-border">User 3</a>
                </div>
            </div>
        </div>
        <a class="user-status-bar_dialog-link user-status-bar_dialog-link--match" onclick="return FastChat.toggleMessageBoxWrapper(<?=$match_id;?>);">
            <i class="fa fa-tourney fa-bolt"></i>
            <div class="user-status-bar_dialog-name">
                Match
            </div>
        </a>
    </div>
</div>
<input placeholder="user_id" type="text" id="user_id" />
<input type="submit" value="Открыть чатик" id="status">
<div class="box box--black">
    <div class="box box--black" id="timer">
        <div class="timers">
            <span class="team-1 time-pool">
                <span>...</span>
            </span>
            <span class="main">...</span>
            <span class="team-2 time-pool">
                <span>...</span>
            </span>
        </div>
    </div>
    <div class="draft" id="draft"></div>
</div>
<script src="js/socket.io-1.4.5.js"></script>
<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/jquery.trackpad-scroll-emulator.min.js"></script>
<script src="js/common.js"></script>
<script src="js/tourney.js"></script>
<script src="js/main.js"></script>
<script src="js/draft.js"></script>
<?php
$SECRET_KEY = '6r16[)7z#g}i?yw{';

$authParams = array(
    'user_id'   => $userId,
    'auth_key'  => md5(uniqid('chat', true)),
    'username'  => 'Peter_'.$userId,
    'mode'      => 2,
    'ava'       => 'no_logo.png',
    'matches'   => 8024,
    'tm'        => time(),
);

$authParams['sig'] = generateSig($authParams, $SECRET_KEY);

function generateSig($parameters, $secretKey)
{
    ksort($parameters);
    $paramsStr = '';
    foreach ($parameters as $k => $v) {
        if ($k != 'matches')
            $paramsStr .= $k . '=' . $v;
    }

    return md5($paramsStr . $secretKey);
}
?>
<script>
    TourneyChat.tryConnect('50c0b', 20);
    Draft.tryConnect(8024);
    Socket.init('<?= http_build_query($authParams);?>', <?=$userId;?>);
</script>
</body>
</html>