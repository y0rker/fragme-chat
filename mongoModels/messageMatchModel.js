var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var MessageMatches = new Schema({
    from_id: { type: Number, required: true },
    match_id: { type: Number, required: true },
    message: { type: String, required: true },
    modified: { type: Date, default: Date.now },
    unread: {type: Boolean, default: true }
});

var MessageMatchesModel = mongoose.model('MessageMatches', MessageMatches);

module.exports.MessageMatchesModel = MessageMatchesModel;