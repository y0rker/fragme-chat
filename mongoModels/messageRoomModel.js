var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var MessageRoom = new Schema({
    room_id: { type: String, required: true },
    user_id: { type: Number, required: true },
    message: { type: String, required: true },
    modified: { type: Date, default: Date.now }
});

var MessageRoomModel = mongoose.model('MessageRoom', MessageRoom);

module.exports.MessageRoomModel = MessageRoomModel;