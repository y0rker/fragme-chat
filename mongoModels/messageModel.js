var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Message = new Schema({
    from_id: { type: Number, required: true },
    to_id: { type: Number, required: true },
    message: { type: String, required: true },
    modified: { type: Date, default: Date.now },
    unread: {type: Boolean, default: true }
});

var MessageModel = mongoose.model('Message', Message);

module.exports.MessageModel = MessageModel;