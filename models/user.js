var config = require('./config');
var arrayHelper = require('../helpers/array');
var urlReplaceHelper = require('../helpers/url');
var jade = require('pug');
var MessageModel = require('../mongoModels/messageModel').MessageModel;
var MessageMatchModel = require('../mongoModels/messageMatchModel').MessageMatchesModel;
var MessageRoomModel = require('../mongoModels/messageRoomModel').MessageRoomModel;
var User = function() {
    this.user_id = 0;
    this.name = "";
    this.avatar = "";
    this.mode = 0;
    this.online = true;
    this.guest = true;
    this.matches = [];
    this.sockets = {
        list: [],
        _inProcess: false,
        _idTimer: null
    };
};
User.prototype.init = function(client, chat) {
    this.guest = client.request.guest;
    if (!this.guest) {
        this.user_id = client.request.user_id;
        this.name = client.request.nick;
        this.avatar = client.request.avatar;
        this.mode = client.request.mode;
    }
    var message_info = {
        user_id: this.user_id,
        username: this.name,
        avatar: this.avatar,
        mode: this.mode
    };
    // Отправляем информацию о себе
    client.emit('info', message_info);
    // Если не авторизован, то выбрасываем
    if (this.guest) return;
    if (client.request.matches != undefined) {
        var matches_arr = client.request.matches;
        if (matches_arr != undefined && matches_arr != 0) {
            this.matches = matches_arr.split(',');
        }
    }

    // Запускаем таймер контроля сокетов
    this._controlSockets(client, chat);

    // Добавляем в массив инфу о пользователе
    chat.users.add(this, client.conn.id);

    // Если мы зашли первый раз (Не обновили страницу)
    if (!chat.timer.exist(this.user_id)) {
        // Посылаем всем ожидающим появления в онлайн
        var answer = {
            user_id: parseInt(this.user_id),
            online: true
        };
        client.broadcast.to(this.user_id + config.SECRET_HASH_ONLINE + config.SECRET_HASH).emit('statusUser', answer);
        // Посыдаем всем ожидающим в матче онлайн
        for (var key_in in this.matches) {
            var answer_m = {};
            answer_m[this.matches[key_in]] = {};
            answer_m[this.matches[key_in]][this.user_id] = true;
            client.broadcast.to(this.matches[key_in] + config.SECRET_HASH_MATCH + config.SECRET_HASH).emit('matchesStatusUsers', answer_m);
        }
    } else {
        // Очищаем задержку выхода (Обновили страницу)
        chat.timer.delete(this.user_id);
    }

    client.join(this.user_id + config.SECRET_HASH);

    // Добавляем прослушку матчей
    for (var key_in in this.matches) {
        client.join(this.matches[key_in] + config.SECRET_HASH_MATCH + config.SECRET_HASH);
    }
};
User.prototype.message = function(client, message) {
    if (/([^\s])/.test(message.message)) {
        if (message.user_id != this.user_id) {
            try {
                var date = new Date;
                var time = (date.getHours() < 10 ? '0' : '') + date.getHours() + ':' + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
                message.nick = this.name;
                message.time = time;
                message.from_id = this.user_id;
                message.message = urlReplaceHelper(message.message);

                /* ЗАПИСЬ В БД */
                var new_message = new MessageModel({
                    to_id: message.user_id,
                    from_id: this.user_id,
                    message: message.message
                });
                new_message.save(function (err) {
                    if (!err) {
                        return new_message;
                    } else {
                        console.log(err);
                        return err;
                    }
                });

                //посылаем сообщение собеседнику
                var our_user_id = message.user_id;
                var fn = jade.compileFile(__dirname + '/../views/fastChat/message_our.jade');
                message.html = fn({
                    user_id: this.user_id,
                    avatar: this.avatar,
                    message: message.message,
                    time: message.time,
                    nick: message.nick
                });
                message.user_id = this.user_id;
                client.broadcast.to(our_user_id + config.SECRET_HASH).emit('message', message);

                // Посылаем сообщение себе
                var message_to_me = {
                    from_id: this.user_id,
                    rand: message.rand,
                    time: time
                };
                client.emit('message', message_to_me);
            } catch (e) {
                console.log(e);
                client.disconnect();
            }
        }
    }
};
User.prototype.messageMatch = function(client, message) {
    if (/([^\s])/.test(message.message)) {
        if (message.match_id != undefined) {
            try {
                message.nick = this.name;
                message.time = (new Date).toLocaleTimeString();
                message.from_id = this.user_id;
                message.message = urlReplaceHelper(message.message);

                /* ЗАПИСЬ В БД */
                var new_message = new MessageMatchModel({
                    match_id: message.match_id,
                    from_id: this.user_id,
                    message: message.message
                });
                new_message.save(function (err) {
                    if (!err) {
                        return new_message;
                    } else {
                        console.log(err);
                        return err;
                    }
                });

                var fn;
                //посылаем сообщение всем в комнате
                fn = jade.compileFile(__dirname + '/../views/fastChat/message_our.jade');
                message.html = fn({
                    user_id: this.user_id,
                    message: message.message,
                    time: message.time,
                    nick: message.nick
                });
                client.broadcast.to(message.match_id + config.SECRET_HASH_MATCH + config.SECRET_HASH).emit('message_match', message);
            } catch (e) {
                console.log(e);
                client.disconnect();
            }
        }
    }
};
User.prototype.messageRoom = function(client, message) {
    if (/([^\s])/.test(message.message)) {
        try {
            message.nick = this.name;
            message.id = this.user_id;
            message.time = (new Date).toLocaleTimeString();
            message.message = urlReplaceHelper(message.message);

            /* ЗАПИСЬ В БД */
            var new_message = new MessageRoomModel({
                room_id: message.roomId,
                user_id: this.user_id,
                message: message.message
            });
            new_message.save(function (err) {
                if (!err) {
                    return new_message;
                } else {
                    console.log(err);
                    return err;
                }
            });

            var fn;
            // Себе
            fn = jade.compileFile(__dirname + '/../views/tourneyChat/message_front.jade');
            var ava = this.avatar;
            message.html = fn({
                user_id: this.user_id,
                message: message.message,
                time: message.time,
                nick: message.nick,
                avatar: ava
            });
            message.can_private = "";
            message.can_admin = "";
            client.emit('message_room', message);

            // Всем
            message.html = fn({
                user_id: this.user_id,
                message: message.message,
                time: message.time,
                nick: message.nick,
                avatar: ava
            });
            fn = jade.compileFile(__dirname + '/../views/tourneyChat/message_can_private.jade');
            message.can_private = fn({
                user_id: this.user_id,
                nick: message.nick,
                avatar: this.avatar
            });
            fn = jade.compileFile(__dirname + '/../views/tourneyChat/message_can_delete.jade');
            message.can_delete = fn({
                id: 0
            });
            client.broadcast.to(message.roomId).emit('message_room', message);
        } catch (e) {
            console.log(e);
            client.disconnect();
        }
    }
};
User.prototype.joinRoom = function(client, message) {
    client.join(message.roomId);
    message.html = '';
    if (this.user_id === undefined) {
        message.guest = true;
    }
    client.emit('message_room', message);
};
User.prototype.joinToDraft = function(client, match_id) {
    client.join(match_id + config.SECRET_HASH_DRAFT + config.SECRET_HASH);
};
User.prototype.readMessages = function(message) {
    if (message.user_id != undefined) {
        MessageModel.update({
            from_id:	parseInt(message.user_id),
            to_id:		parseInt(this.user_id)
        },{
            unread: false
        },{
            multi: true
        }, function(err, num) {
            if (err) console.log(err);
        });
    }
};
User.prototype.getStatus = function(client, chat, message) {
    if (message.user_id != undefined) {
        // Получаем статус пользователя
        var status = chat.users.getStatus(message.user_id);
        var answer = {
            user_id : message.user_id,
            online : status
        };
        // Отправляем статус запросителю
        client.emit('statusUser', answer);
        // Подписываемся на изменение статуса пользователя
        client.join(message.user_id + config.SECRET_HASH_ONLINE + config.SECRET_HASH);
    }
};
User.prototype.getStatusesUsersMatches = function(client, chat, message) {
    if (this.guest) return;
    var message_return = {};
    for (var match_id in message) {
        var users = message[match_id];
        message_return[match_id] = {};
        for (var i = 0; i < users.length; i++) {
            message_return[match_id][users[i]] = chat.users.getStatus(users[i]);
        }
    }
    client.emit('matchesStatusUsers', message_return);
};
User.prototype.unSubscribeMe = function(client, message) {
    if (message.user_id != undefined)
        client.leave(message.user_id + config.SECRET_HASH_ONLINE +config.SECRET_HASH, function () {
            
        });
};
User.prototype._controlSockets = function(client, chat) {
    var user = this;
    user.sockets._idTimer = setInterval(function() {
        if (!user.sockets._inProcess) {
            user._inProcess = true;
            var array = user.sockets.list;
            var socketList = chat.io.sockets.server.eio.clients;
            for (var socket in array) {
                if (socketList[array[socket]] === undefined) {
                    arrayHelper(array, array[socket]);
                    user.disconnect(client, chat);
                }
            }
            user._inProcess = false;
        }
    }, config.TIME_CONTROL_USER_SOCKETS);
};
User.prototype.disconnect = function(client, chat) {
    if (this.guest) return;
    var user = this;
    // Удаляем его socketId из массива соединений
    chat.users.removeSocketId(user.user_id, client.conn.id);

    // Проверяем, нет ли активных сокетов у данного пользователя
    var countSockets = chat.users.getCountSockets(user.user_id);

    // Если активных сокетов нет, то значит пользователь вышел из всех вкладок
    if (countSockets == 0) {
        // Ставим счетчик на выход пользователя
        chat.timer.set(user.user_id, makeOffline);

        function makeOffline() {
            // Убиваем счетчик сокетов
            clearInterval(user.sockets._idTimer);
            // Ставим в очередь на рассылку оффлайн
            chat.queueQuit.addUser(user.user_id + config.SECRET_HASH_ONLINE + config.SECRET_HASH, false, user.user_id);
            // Посылаем так же оффлайн в матчи
            for (var key_m in user.matches) {
                chat.queueQuit.addMatch(user.matches[key_m] + config.SECRET_HASH_MATCH + config.SECRET_HASH, false, user.user_id, user.matches[key_m]);
            }
            // Убиваем счетчик выхода
            chat.timer.delete(user.user_id);
            // Запоминаем что мы оффлайн
            chat.users.offline(user);
        }
    }
};
module.exports = User;