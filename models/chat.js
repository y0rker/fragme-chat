var auth = require('./auth');
var timer = require('../helpers/timer');
var arrayHelper = require('../helpers/array');
var config = require('./config');
var mongoose = require('../libs/mongoose');
var chat = require('../models/chat');
var redis = require('socket.io-redis');
var redisSimple = require('redis');
var Settings = require('../helpers/settings');
var mysql = require('mysql');
var Chat = {
    io: null,
    auth: auth,
    timer: new timer(),
    pool: null,
    queueQuit: {
        _list: [],
        _in_process: false,
        _process: function() {
            // Рассылаем статус офлай
            if (Chat.queueQuit._in_process == false) {
                Chat.queueQuit._in_process = true;
                var array = Chat.queueQuit._list;
                for (var key in array) {
                    if (array.hasOwnProperty(key)) {
                        var user_id = parseInt(array[key].user_id);
                        switch (array[key].type) {
                            // Всем пользователям, открывшим личный чат
                            case 'user':
                                var answer_u = {
                                    user_id: user_id,
                                    online: array[key].status
                                };
                                Chat.io.sockets.in(array[key].room).emit('statusUser', answer_u);
                                Chat.queueQuit._removeUser(user_id);
                            break;
                            // Всем пользователям, кто в одном матче с текущим пользователем
                            case 'match':
                                var match_id = parseInt(array[key].match_id);
                                var answer_m = {};
                                answer_m[match_id] = {};
                                answer_m[match_id][parseInt(user_id)] = array[key].status;
                                Chat.io.sockets.in(array[key].room).emit('matchesStatusUsers', answer_m);
                                Chat.queueQuit._removeMatch(match_id);
                            break;
                        }
                    }
                }
                Chat.queueQuit._in_process = false;
            }
        },
        addUser: function(room, status, user_id) {
            this._list['user_' + user_id] =  {
                room: room,
                user_id: user_id,
                status: status,
                type: 'user'
            };
        },
        _removeUser: function(user_id) {
            delete this._list['user_' + user_id];
        },
        addMatch: function(room, status, user_id, match_id) {
            this._list['match_' + match_id] =  {
                room: room,
                user_id: user_id,
                match_id: match_id,
                status: status,
                type: 'match'
            };
        },
        _removeMatch: function(match_id) {
            delete this._list['match_' + match_id];
        }
    },
    init: function(io) {
        this.io = io;
        io.adapter(redis({ prefix: Settings.redis.prefix, host: Settings.redis.host, port: Settings.redis.port }));

        // Инициализируем приемники от php скрипта
        var redisClient = redisSimple.createClient();
        // Посылаем сингнал, что появились драфты
        redisClient.subscribe('draft.updateMatch');
        redisClient.on("message", function(channel, message) {
            message = JSON.parse(message);
            io.to( message.match_id + config.SECRET_HASH_DRAFT + config.SECRET_HASH).emit('draft.updateMatch');
        });
        // Инициализируем приемники от php скрипта
        var redisClientMatches = redisSimple.createClient();
        // Посылаем сингнал, что появились драфты
        redisClientMatches.subscribe('draft.popup');
        redisClientMatches.on("message", function(channel, message) {
            message = JSON.parse(message);
            var users = message.users;
            for (var key in users) {
                io.to(users[key] + config.SECRET_HASH).emit('draft.popup');
            }
        });
        io.use(this.auth.isAuth);
        // Отправляем и очищаем очередь отправки статусов (Раз в config.TIME_TO_QUIT секунд)
        setInterval(this.queueQuit._process, config.TIME_TO_QUIT);
        // Mongo init
        mongoose();
        this.pool  = new mysql.createPool({
            connectionLimit 	: Settings.mysql.connectionLimit,
            host            	: Settings.mysql.host,
            user 				: Settings.mysql.user,
            password 			: Settings.mysql.password,
            database 			: Settings.mysql.db
        });
    },
    users: {
        list: [],
        add: function(user, socketId) {
            var short_el = Chat.users._getShortElByUserId(user.user_id);
            // Добавляем или обновляем инфу о пользователе
            if (this.list[short_el] !== undefined) {
                this.list[short_el].online = true;
                this.list[short_el].avatar = user.avatar;
                this.list[short_el].mode = user.mode;
            } else {
                this.list[short_el] = user;
            }
            this.list[short_el].sockets.list.push(socketId);
        },
        remove: function(user) {
            var short_el = Chat.users._getShortElByUserId(user.user_id);
            delete this.list[short_el];
        },
        online: function(user) {
            var short_el = Chat.users._getShortElByUserId(user.user_id);
            if (this.list[short_el] !== undefined) {
                this.list[short_el].online = true;
            }
        },
        offline: function(user) {
            var short_el = Chat.users._getShortElByUserId(user.user_id);
            if (this.list[short_el] !== undefined) {
                this.list[short_el].online = false;
            }
        },
        getStatus: function(user_id) {
            var short_el = Chat.users._getShortElByUserId(user_id);
            var user = this.list[short_el];
            if (user != undefined) {
                if (user.online == true) {
                    return true;
                }
            }
            return false;
        },
        removeSocketId: function(user_id, socketId) {
            var short_el = Chat.users._getShortElByUserId(user_id);
            if (this.list[short_el] !== undefined) {
                arrayHelper(this.list[short_el].sockets.list, socketId);
            }
        },
        getCountSockets: function(user_id) {
            var short_el = Chat.users._getShortElByUserId(user_id);
            if (this.list[short_el] !== undefined) {
                return this.list[short_el].sockets.list.length;
            }
            return 0;
        },
        _getShortElByUserId: function(user_id) {
            return 'user_' + user_id;
        }
    }
};
module.exports = Chat;