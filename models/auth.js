var md5 = require('md5');
var config = require('./config');
var Auth = {
    isAuth: function (socket, next) {
        var handshakeData = socket.request;
        var required = [];
        if (handshakeData._query["mode"] == 0)
            required = ["auth_key", "mode", "tm", "sig"];
        else
            required = ["auth_key", "ava", "mode", "tm", "user_id", "username", "sig"];
        var strToHash = '';
        for (var i = 0; i < required.length; i++) {
            var paramName = required[i];
            if (!handshakeData._query[paramName]) {
                next(new Error('not authorized'));
                return;
            }

            if (paramName != "sig" && paramName != 'matches')
                strToHash += paramName + '=' + handshakeData._query[paramName];
        }

        var sig = handshakeData._query.sig;

        strToHash += config.SECRET_KEY;

        var hash = md5(strToHash);
        if (hash == sig) {
            handshakeData.guest = true;
            if (handshakeData._query.mode == 0) {
                next();
                return;
            }
            if (handshakeData._query.ava == 'no_logo.png') {
                handshakeData.avatar = '/images/no_logo.png';
            } else {
                handshakeData.avatar = config.PATH_IMG + handshakeData._query.ava;
            }
            handshakeData.guest = false;
            handshakeData.matches = handshakeData._query.matches;
            handshakeData.online = true;
            handshakeData.nick = handshakeData._query.username;
            handshakeData.user_id = handshakeData._query.user_id;
            handshakeData.mode = handshakeData._query.mode;

            next();

            return;
        }

        next(new Error('not authorized'));
    }
};
module.exports = Auth;